# UberTimeSeries

[](url)


## Pronostico de Demanda de Uber mediante análisis de Series en el tiempo


El presente análisis busca obtener un pronóstico de la demanda de viajes para la aplicacion Uber en la ciudad de Nueva York mediante el análisis de series en el tiempo con datos abiertos provistos por la misma plataforma


## Presentación
https://youtu.be/xQUZh8fCxq0




## Authors and acknowledgment
Grupo 6.

Jose Cattaneo.
Alvaro Alvarez.
Miguel del Puerto.
Ivan Fretes.

## DataSet
https://www.kaggle.com/fivethirtyeight/uber-pickups-in-new-york-city

